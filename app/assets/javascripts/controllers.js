'use strict';

var taskControllers = angular.module('taskControllers', []);

taskControllers.controller('TaskListCtrl', ['$scope', '$http', 
  function ($scope, $http) {
    $http.get('tasks/all_json').success(function(data){
      $scope.tasks = data;
    });

    $scope.taskNameFilter = function(taskItem){
      return taskItem.title;
    }
}]);

taskControllers.controller('TaskDetailCtrl', ['$scope', '$http', '$routeParams', 
  function ($scope, $http, $routeParams) {
    $http.get('edit_json/' + $routeParams.taskId).success(function(data){
      $scope.task = data;
    });

    $scope.taskNameFilter = function(taskItem){
      return taskItem.title;
    }
}])
/* Controllers */

angular.module('directive', []).directive('contenteditable', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      // вид -> модель
      elm.bind('blur', function() {
        scope.$apply(function() {
          ctrl.$setViewValue(elm.html());
        });
      });
 
      // модель -> вид
      ctrl.$render = function(value) {
        elm.html(value);
      };
 
      // загрузка начального значения из DOM
      ctrl.$setViewValue(elm.html());
    }
  };
});
