class TasksController < ApplicationController
	def index
		
	end

	def index_json
		@tasks = Task.all
		render :json => @tasks
	end

	def show
		render layout: false
	end

	def show_json
		@task = Task.find(params[:id])
		render :json => @task
	end

	def all
		render layout: false
	end

	def all_json
		@tasks = Task.all
		render :json => @tasks
	end

	def new
		@task = Task.new
	end

	def edit 
		render layout: false
	end

	def edit_json
		@task = Task.find(params[:id])
		render :json => @task
	end

	def create
		@task = Task.new(task_params)
		
		if @task.save
			redirect_to @task
		else
			render 'new'
		end
	end

	def update
		@task = Task.find(params[:id])
		 
		if @task.update(task_params)
			redirect_to @tasks_path
		else
			render 'edit'
		end 
	end

	def destroy
		@task = Task.find(params[:id])
		@task.destroy

		redirect_to tasks_path
	end

	private
		def task_params
			params.require(:task).permit(:title)
		end
end 